<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    </head>
    <body>
        <div class="p-4">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4>Inquiry Billing</h4>
                            <form action="{{ route('update-va', $transaction->id) }}" method="post">
                                @csrf
        
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email address</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" value="{{ auth()->user()->email }}" readonly>
                                </div>
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="example" value="{{ auth()->user()->name }}" readonly>
                                </div>
                                <div class="mb-3">
                                    <label for="phone_number" class="form-label">Phone number</label>
                                    <input type="text" class="form-control" id="phone_number" name="name" placeholder="081234567890" value="{{ auth()->user()->phone_number }}" readonly>
                                </div>
                                <div class="mb-3">
                                    <label for="virtual_account" class="form-label">Virtual Account</label>
                                    <input type="text" class="form-control" id="virtual_account" name="virtual_account" value="{{ $transaction->payment->virtual_account }}" readonly>
                                </div>
                                <div class="mb-3">
                                    <label for="amount" class="form-label">Amount</label>
                                    <input type="text" class="form-control" id="amount" name="amount" value="{{ $transaction->payment->amount }}">
                                </div>
                                <a href="{{ route('inquiry', $transaction->id) }}" class="btn btn-secondary w-100 mb-2">Inquiry</a>
                                <button class="btn btn-primary w-100" type="submit">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    </body>
</html>

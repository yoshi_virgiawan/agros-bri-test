<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BriToken extends Model
{
    protected $fillable = [
        'token',
        'limit',
    ];
}

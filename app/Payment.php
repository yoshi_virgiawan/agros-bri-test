<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'transaction_id',
        'virtual_account',
        'amount',
        'expired_date',
        'status',
        'payment_date',
        'request_id',
        'customer_number',
        'channel_type',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
}

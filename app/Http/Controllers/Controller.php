<?php

namespace App\Http\Controllers;

use App\BriToken;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getBriToken()
    {
        $now = Carbon::now();
        $token = BriToken::orderBy('created_at', 'DESC')->first();
        if($token) {
            $isExpired = $now->gte(Carbon::parse($token->created_at)->addSeconds($token->limit));
            if($isExpired) {
                $token = $this->getAccessTokenFromBRI();
            }
        } else {
            $token = $this->getAccessTokenFromBRI();
        }

        return $token->token;
    }

    public function getAccessTokenFromBRI()
    {
        $url = env('BRI_API_URL') . '/oauth/client_credential/accesstoken?grant_type=client_credentials';
        $body = 'client_id='.env('BRI_CONSUMER_KEY').'&'.'client_secret='.env('BRI_CONSUMER_SECRET');

        $curl = curl_init();
    
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "content-type: application/x-www-form-urlencoded",
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response_body = json_decode($response);

            $token = new BriToken();
            $token->token = $response_body->access_token;
            $token->limit = $response_body->expires_in;
            $token->save();

            return $token;
        }
    }
}

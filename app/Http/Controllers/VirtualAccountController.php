<?php

namespace App\Http\Controllers;

use App\User;
use App\Payment;
use Carbon\Carbon;
use App\Transaction;
use App\Helpers\BniEnc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VirtualAccountController extends Controller
{
    public function createVa()
    {
        return view('create-va');
    }
    
    public function store(Request $request)
    {
        DB::beginTransaction();
        $user = User::find(auth()->user()->id);
        $token = $this->getBriToken();
        $secret_key = env('BRI_CONSUMER_SECRET');
        $authorization = "Bearer {$token}";
        $path = '/v1/briva';
        $create_va_endpoint = env('BRI_API_URL') . $path;
        $method = 'POST';
        $tomorrow = Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
        
        $institution_code = 'J104408';
        $briva_no = '77777';
        $customer_number = $this->generateCustomerNumber();

        $transaction = $user->transactions()->create([
            // 'reference' => $reference,
            'amount' => $request->amount,
        ]);

        $reference = 'PO-' . date('Ym') . '-' . sprintf("%06s", $transaction->id);

        $transaction->update([
            'reference' => $reference,
        ]);

        $body = [
            "institutionCode" => $institution_code,
            "brivaNo" => $briva_no,
            "custCode" => strval($customer_number),
            "nama" => $user->name,
            "amount" => $transaction->amount,
            "keterangan" => 'BRIVA Testing',
            "expiredDate" => $tomorrow,
        ];
        
        $curl = curl_init();

        $now = Carbon::now()->setTimezone('UTC')->toIso8601ZuluString('millisecond');
        $payload = "path={$path}&verb={$method}&token={$authorization}&timestamp={$now}&body=" . json_encode($body, JSON_UNESCAPED_SLASHES);
        $payload_hashed = hash_hmac('sha256', $payload, $secret_key, true);
        $signature = base64_encode($payload_hashed);
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $create_va_endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($body, JSON_UNESCAPED_SLASHES),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "Accept: */*",
                "Authorization: " . $authorization,
                "Content-Type: application/json",
                "BRI-Timestamp: " . $now,
                "BRI-Signature: " . $signature,
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response_body = json_decode($response);
            if(isset($response_body->responseCode)) {
                $data = $response_body->data;
                $transaction->payment()->create([
                    'virtual_account' => $data->brivaNo . $data->custCode,
                    'amount' => $data->amount,
                    'expired_date' => $data->expiredDate,
                    'status' => 'pending',
                    'customer_number' => $customer_number,
                ]);
            } else {
                DB::rollBack();
                dd($response_body);
            }

            DB::commit();
            return redirect()->route('transaction', $transaction->id);
        }
    }
    
    function generateCustomerNumber() {
        $number = mt_rand(1000000000, 9999999999); // better than rand()
    
        // call the same function if the barcode exists already
        if ($this->customerNumberExists($number)) {
            return $this->generateCustomerNumber();
        }
    
        // otherwise, it's valid and can be used
        return $number;
    }
    
    function customerNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Payment::where('customer_number', $number)->exists();
    }
}

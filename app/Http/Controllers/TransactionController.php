<?php

namespace App\Http\Controllers;

use App\Helpers\BniEnc;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function show($id)
    {
        $transaction = Transaction::find($id);
        return view('inquiry', compact('transaction'));
    }

    public function callback(Request $request)
    {
        $client_id = '70081'; //client id from BNI
		$secret_key = '9c6b54eb8cc4715b5bd54d72d1bd48d2'; // secret key from BNI
        $prefix = '988';

        if($request->client_id) {
            $data = BniEnc::decrypt($request->data, $client_id, $secret_key);

            if(!$data) {
                return response()->json([
                    'status' => '999',
                    'message' => 'waktu server tidak sesuai NTP atau secret key salah.',
                ]);
            } else {
                $cumulative_payment_amount = $data['cumulative_payment_amount'];
                $customer_name = $data['customer_name'];
                $payment_amount = $data['payment_amount'];
                $trx_id = $data['trx_id'];
                $payment_ntb = $data['payment_ntb'];
                $trx_amount = $data['trx_amount'];
                $virtual_account = $data['virtual_account'];
                $datetime_payment = $data['datetime_payment'];
                $datetime_payment_iso8601 = $data['datetime_payment_iso8601'];

                $transaction = Transaction::with(['payment'])->where('reference', $trx_id)->first();
                $payment = $transaction->payment;
                $payment->status = 'success';
                $payment->payment_date = $datetime_payment;
                $payment->save();

                return response()->json([
                    'status' => '000',
                ]);
            }
        }
    }
}

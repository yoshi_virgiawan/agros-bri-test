<?php

namespace App\Http\Controllers;

use App\Helpers\BniEnc;
use Illuminate\Http\Request;

class EncryptController extends Controller
{
    public function encrypt()
    {
        // 3.1 Create Invoice/Billing
        $data = [
            "type" => "createbilling",
            "client_id" => "70081",
            "trx_id" => "PO-AGRS-220416.912379",
            "trx_amount" => "250000",
            "billing_type" => "c",
            "customer_name" => "Agung Pambudi",
            "customer_email" => "product@agros.co.id",
            "customer_phone" => "085853352902",
            "datetime_expired" => "2022-08-10T17:00:00+07:00",
            "description" => "Payment of Trx PO-AGRS-220416.912379",
        ];

        // 3.2 Inquiry Invoice/Billing
        // $data = [
        //     'type' => 'inquirybilling',
        //     'client_id' => '70081',
        //     'trx_id' => 'PO-AGRS-220416.912378',
        // ];

        // 3.3 Update Transaction
        // $data = [
        //     'client_id' => '70081',
        //     'trx_id' => 'PO-AGRS-220416.912377',
        //     'trx_amount' => '100000',
        //     'customer_name' => 'Agung Pambudi',
        //     'customer_email' => 'agung@gmail.com',
        //     'customer_phone' => '085853352902',
        //     'datetime_expired' => '2022-08-10T17:00:00+07:00',
        //     'description' => 'Payment of Trx PO-AGRS-220416.912377',
        //     'type' => 'updateBilling'
        // ];

        // 3.4 Transaction Payment Notification (BNI eCollection system to Biller Client POST JSON)
        // $data = [
        //     'trx_id' => 'PO-AGRS-220416.912378',
        //     'virtual_account' => '9887008122080903',
        //     'customer_name' => 'Agung Pambudi',
        //     'trx_amount' => '250000',
        //     'payment_amount' => '250000',
        //     'cumulative_payment_amount' => '250000',
        //     'payment_ntb' => '446864',
        //     'datetime_payment' => '2022-08-09 15:20:38',
        //     'datetime_payment_iso8601' => '2022-08-09T15:20:38+07:00',
        // ];

        $client_id = '70081'; //client id from BNI
		$secret_key = '9c6b54eb8cc4715b5bd54d72d1bd48d2'; // secret key from BNI

        $hashdata = BniEnc::encrypt($data, $client_id, $secret_key);

        echo '<p style="line-break: anywhere;">' . $hashdata . '</p>';
    }

    public function decrypt()
    {
        $hashdata = 'FihFGU0WG0tIIUIVBltbXnhOdj4gDWQ2F1YvZGcZG00SHUwdRx4VT0YmSzUYCktRe1IBD0RNAAojNhhKRSMZNhYMEE8EYVp9fkd8d05WVlsHDkw2Jh1THhJMGkpGFiBNGSJDITUYBgkMYXl0UV5XWgYHVDceGUQfE0sDPj5IZAdWYAFSBERadwZNNVUHL0haCE05ZEZZd2NGfQs_PUddD1dfAEsMRVACc1kAPB4JWVgISQd_Wit1TlwEW0B3WxcERgtHCXxaVxECXHgFS1hQVABSNVUHFFQdIUkUTUodIUQcDEYIdk5ZAQZRCHlKSlNSdGB3eA8fPRkSThRGRBwdRSIJRCNNHR1PSh88PwZTQ2B3WQMCUUN5Z1MEU3cAByk2FCFGHz8VJENKGDNMHCgRFVQWSTYRDnlPVnldfAhJRwxEaQBLCFoNTzQiREwWFBkeRhVLPBcgThkaTxtKNhgLf0NdAFsCUkl8f1AGB0tdWElzYXh_Bx4-GhlGE0dFIxVEIwpLG0weHlZCHj1ADVdCZgBRAAhMUw9JBFYETUgaKEsdCz8QA01dAn9XDnJFXFFhAFk8Tw4WSR8TSxE0SAdldUFkCE4GWl84UwpFPREQRUoNSQt9VWB-XVA2IzVrRWEJSF4HBglMC2kEaDRqMxQqLWs4P04YG0gYIEMhQ0YfIFMEFT1JAlFQBgFWcgdlWE0HTA92PREGAEldeVUDAlBHd1xPe1p3UUQFBVdTShsYAyc1HkJGHxJLHw9MG21FGSpGIiNDJD4cG09KGTw_BlNDYHdZAwJRQ3lnUwRTdwBEWAdRKUodQwcmOEsYRU0SHhkSSxZtRRwmRR4cRBk-SxsiTBMSPwh-R196BlkBf0NXSl8GSgAQRVQHViJLGEM2JgtNEhtNFEkdEU1MQ0RIJhoYH0UlPkscHkwYC0ADfnZfTQhTV39Ffk5YEHFdC3hMW0ZRclUFAyUbSxgEVgRLRBYiQhkhQB5MQBVKVBpTTRQlDRxJH0pFDmE';

        $client_id = '70081'; //client id from BNI
		$secret_key = '9c6b54eb8cc4715b5bd54d72d1bd48d2'; // secret key from BNI
		
		$parsedata = BniEnc::decrypt($hashdata, $client_id, $secret_key);

        dd($parsedata);
    }
}

<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    public function notification(Request $request)
    {
        if($this->signatureIsValid($request)) {
            $va = $request->brivaNo;
            $amount = $request->billAmount;
            $payment = Payment::where('virtual_account', $va)->first();
            if($payment) {
                if($amount == $payment->amount) {
                    $payment_date = $request->transactionDateTime;
                    $channel_type = null;
                    switch ($request->terminalId) {
                        case 1:
                            $channel_type = 'Teller';
                            break;
                        case 2:
                            $channel_type = 'ATM';
                            break;
                        case 3:
                            $channel_type = 'IB';
                            break;
                        case 4:
                            $channel_type = 'SMSB';
                            break;
                        case 5:
                            $channel_type = 'CMS';
                            break;
                        case 6:
                            $channel_type = 'EDC';
                            break;
                        case 7:
                            $channel_type = 'RTGS';
                            break;
                        case 8:
                            $channel_type = 'OTHER';
                            break;
                        case 9:
                            $channel_type = 'API';
                            break;
                        default:
                            $channel_type = null;
                            break;
                    }
                    
                    $payment->request_id = $request->journalSeq;
                    $payment->channel_type = $channel_type;
                    $payment->payment_date = $payment_date;
                    $payment->status = 'success';
                    $payment->save();

                    Log::info('RESPONSE: ' . json_encode([
                        'responseCode' => '0000',
                        'responseDescription' => 'Success',
                    ]));
    
                    return response()->json([
                        'responseCode' => '0000',
                        'responseDescription' => 'Success',
                    ]);
                } else {
                    Log::info('RESPONSE: ' . json_encode([
                        'responseCode' => '0102',
                        'responseDescription' => 'Invalid Signature',
                    ]));
                    
                    return response()->json([
                        'responseCode' => '0102',
                        'responseDescription' => 'Invalid Signature',
                    ]);
                }
            } else {
                Log::info('RESPONSE: ' . json_encode([
                    'responseCode' => '0102',
                    'responseDescription' => 'Invalid Signature',
                ]));

                return response()->json([
                    'responseCode' => '0102',
                    'responseDescription' => 'Invalid Signature',
                ]);
            }
        } else {
            Log::info('RESPONSE: ' . json_encode([
                'responseCode' => '0102',
                'responseDescription' => 'Invalid Signature',
            ]));

            return response()->json([
                'responseCode' => '0102',
                'responseDescription' => 'Invalid Signature',
            ]);
        }
    }

    public function signatureIsValid(Request $request): bool
    {
        $signature = $request->header('BRI-Signature');
        $timestamp = $request->header('BRI-Timestamp');
        if (!$signature) {
            return false;
        }

        $secret_key = env('BRI_CONSUMER_KEY');
        $consumer_secret = env('BRI_CONSUMER_SECRET');

        if (!$secret_key) {
            return false;
        }

        $body = $request->getContent();
        $token = "Bearer {$secret_key}";
        $path = route('bri.notification');
        $method = 'POST';
        $correct_payload = "path={$path}&verb={$method}&token={$token}&timestamp={$timestamp}&body={$body}";
        $correct_signature = base64_encode(hash_hmac('sha256', $correct_payload, $consumer_secret, true));

        Log::info('body: ' . $body);
        Log::info('signature: ' . $signature);
        Log::info('timestamp: ' . $timestamp);
        Log::info('correct_payload: ' . $correct_payload);
        Log::info('correct_signature: ' . $correct_signature);

        if ($signature === $correct_signature) {
            return true;
        }

        return false;
    }
}

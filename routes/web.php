<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'VirtualAccountController@createVa');
    Route::get('transaction/{id}', 'TransactionController@show')->name('transaction');
    Route::post('create-va', 'VirtualAccountController@store')->name('create-va');
    Route::get('inquiry/{id}', 'VirtualAccountController@inquiry')->name('inquiry');
    Route::post('update-va/{id}', 'VirtualAccountController@update')->name('update-va');
});

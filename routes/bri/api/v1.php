<?php

use Illuminate\Support\Facades\Route;

Route::post('notification', 'API\TransactionController@notification')->name('bri.notification');
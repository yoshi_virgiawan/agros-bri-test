<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'Yoshi Virgiawan',
            'email' => 'yoshivirgiawan09@gmail.com',
            'password' => Hash::make('yoshi2001'),
            'phone_number' => '081270203899',
        ];

        User::create($data);
    }
}
